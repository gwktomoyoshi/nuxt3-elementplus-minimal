import { defineNuxtPlugin } from '#app'
import ElementPlus from 'element-plus/dist/index.full'

export default defineNuxtPlugin((nuxtApp) => {
  const ElUpload = nuxtApp.vueApp._context.components.ElUpload
  if (ElUpload) { return }
  nuxtApp.vueApp.use(ElementPlus)
})
