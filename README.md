Nuxt3 + Element Plus minimal starter
====================================

# How to make this env with commandline

  1. Create project and install molules.

```bash
$ npx nuxi init nuxt3-elementplus-minimal
$ cd nuxt3-elementplus-minimal
$ npm install element-plus sass --save
```

  2. Add/Change files in project.

    * assets/css/elementPlus.scss
    * pages/index.vue
    * plugins/elementPlus.ts
    * app.vue
    * nuxt.config.ts

