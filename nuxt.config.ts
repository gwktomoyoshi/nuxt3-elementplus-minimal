import { defineNuxtConfig } from 'nuxt/config'

const lifecycle = process.env.npm_lifecycle_event

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  meta: [{ name: 'viewport', content: 'width=device-width, initial-scale=1' },],
  css: ['~/assets/css/elementPlus.scss'],
  build: {
    transpile: lifecycle === 'build' ? ['element-plus'] : []
  },
  plugins: ['~/plugins/elementPlus.ts']

})
